package master.Main;

import model.Book;
import service.BookPagesServiceImp;
import service.BookServiceImp;
import service.ConsolePrinterServiceImp;
import service.TextFileService;

public class Main {

    public static void main(String[] args) {
        BookServiceImp bookServiceImp = new BookServiceImp();
        BookPagesServiceImp bookPagesServiceImp = new BookPagesServiceImp();
        bookPagesServiceImp.setFileService(new TextFileService());
        bookServiceImp.setBookPagesService(bookPagesServiceImp);
        ConsolePrinterServiceImp consolePrinterServiceImp = new ConsolePrinterServiceImp();

        Book book = bookServiceImp.createBook("t.txt",3,10);//путь, количество строк на странице, количество символов в строке

        consolePrinterServiceImp.printBook(book,1,14);
    }
}










