package model;

import service.BookPagesService;

import java.util.List;

public class Book {
    List<Page> pages;
    int countOfLines;
    int countOfSymbols;

    public Book(int countOfLines, int countOfSymbols, List<Page> pages) {
        this.countOfLines = countOfLines;
        this.countOfSymbols = countOfSymbols;
        this.pages=pages;
    }

    public int getCountOfLines() {
        return countOfLines;
    }

    public int getCountOfSymbols() {
        return countOfSymbols;
    }

    public List<Page> getPages() {
        return pages;
    }
}
