package model;

public class Page {
    String[] lines;
    int pageNumber;

    public Page(String[] lines, int number) {
        this.lines = lines;
        this.pageNumber = number;
    }

    public String getLine(int numLine) {
        return lines[numLine];
    }

    public int getNumber() {
        return pageNumber;
    }
}
