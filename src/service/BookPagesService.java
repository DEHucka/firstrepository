package service;

import model.Page;

import java.util.List;

public interface BookPagesService {
    List<Page> create(String path, int countOfLines, int countOfSymbols);
}
