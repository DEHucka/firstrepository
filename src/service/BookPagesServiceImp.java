package service;

import model.Page;

import java.util.ArrayList;
import java.util.List;

public class BookPagesServiceImp implements BookPagesService{
    FileService fileService;

    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public List<Page> create(String path, int countOfLines, int countOfSymbols) {
        List<Page> pages = new ArrayList<>();
        String[] lines = new String[countOfLines];
        String allText = fileService.read(path);
        int currentPage = 1;
        int currentNumLine = 0;
        String word = "";
        char letter;

        for (int i = 0; i < countOfLines; i++) {
            lines[i] = "";
        }

        while (!allText.equals("")) {
            letter = allText.charAt(0);
            if (letter == ' ') {
                if (lines[currentNumLine].length() + word.length() >= countOfSymbols) {
                    currentNumLine++;
                }
                if (currentNumLine >= countOfLines) {
                    pages.add(new Page(lines, currentPage));
                    currentPage++;
                    lines = new String[countOfLines];
                    for (int i = 0; i < countOfLines; i++) {
                        lines[i] = "";
                    }
                    currentNumLine = 0;
                }
                if (lines[currentNumLine].length() == 0) lines[currentNumLine] = word;
                else lines[currentNumLine] += " " + word;
                word = "";
            } else
                word += letter;
            allText = allText.substring(1);
        }
        pages.add(new Page(lines, currentPage));
        return pages;
    }
}
