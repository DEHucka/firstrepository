package service;

import model.Book;

public interface BookService {
    Book createBook (String path, int countOfLines, int countOfSymbols);
}
