package service;

import model.Book;
import model.Page;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BookServiceImp implements BookService {
    BookPagesService bookPagesService;

    @Override
    public Book createBook(String path, int countOfLines, int countOfSymbols) {
        return new Book(countOfLines, countOfSymbols, bookPagesService.create(path, countOfLines, countOfSymbols));
    }

    public void setBookPagesService(BookPagesService bookPagesService) {
        this.bookPagesService = bookPagesService;
    }
}
