package service;

import model.Book;
import model.Page;

import java.util.List;

public class ConsolePrinterServiceImp implements PrinterService {

    @Override
    public void printBook(Book book, int numPage, int countOfPages) {
        String[] forPrint = stringsForPrint(numPage,countOfPages,
                book.getPages(),book.getCountOfLines(),book.getCountOfSymbols());
        for (var line :
                forPrint) {
            System.out.println(line);
        }
    }

    String[] stringsForPrint(int numPage, int countOfPages,
                          List<Page> pages, int countOfLines, int countOfSymbols) {
        String[] forPrint = new String[countOfLines + 3];
        for (var page :
                pages) {
            if (page.getNumber() >= numPage & page.getNumber() < numPage + countOfPages) {
                if (forPrint[0] == null) forPrint[0] = "┌";
                else forPrint[0] += "┌";
                for (int i = 1; i < countOfSymbols + 1; i++) forPrint[0] += "─";
                forPrint[0] += "┐";
                for (int i = 1; i < countOfLines + 1; i++) {
                    String pageLine = page.getLine(i - 1);
                    if (forPrint[i] == null) forPrint[i] = "│" + pageLine;
                    else forPrint[i] += "│" + pageLine;
                    for (int j = pageLine.length(); j < countOfSymbols; j++) forPrint[i] += " ";
                    forPrint[i] += "│";
                }
                if (forPrint[countOfLines + 1] == null) forPrint[countOfLines + 1] = "│";
                else forPrint[countOfLines + 1] += "│";
                for (int i = 0; i < countOfSymbols - Integer.toString(page.getNumber()).length() - 1; i++)
                    forPrint[countOfLines + 1] += " ";
                forPrint[countOfLines + 1] += Integer.toString(page.getNumber());
                forPrint[countOfLines + 1] += " │";
                if (forPrint[countOfLines + 2] == null) forPrint[countOfLines + 2] = "└";
                else forPrint[countOfLines + 2] += "└";
                for (int i = 1; i < countOfSymbols + 1; i++) forPrint[countOfLines + 2] += "─";
                forPrint[countOfLines + 2] += "┘";
            }
        }

        return forPrint;
    }
}
