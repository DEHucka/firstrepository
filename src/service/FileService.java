package service;

public interface FileService {
    String read(String path);
}
