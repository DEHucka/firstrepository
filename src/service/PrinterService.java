package service;

import model.Book;

public interface PrinterService {
    void printBook(Book book, int numPage, int countOfPages);
}
